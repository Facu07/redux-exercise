import { useDispatch, useSelector } from 'react-redux';

import {operations} from 'state/actions';
import { selectCurrentNumber, selectCurrentStack } from 'state/selectors';

import styles from './Calculator.module.css';

const renderStackItem = (value, index) => {
  return <div key={index}>{value}</div>;
};

export const Calculator = () => {
  const currentNumber = useSelector(selectCurrentNumber);
  const stack = useSelector(selectCurrentStack);

  const dispatch = useDispatch();
  const onClickNumber = (number) => {
    const action = operations('', number);
    dispatch(action);
  };
  const onClick = (operator, number) => {
    const action = operations(operator, number);
    dispatch(action);
  };

  return (
      <div className={styles.main}>
        <div className={styles.display}>{currentNumber}</div>
        <div className={styles.numberKeyContainer}>
          {[...Array(9).keys()].map((i) => (
              <button key={i} onClick={() => onClickNumber(i + 1)}>
                {i + 1}
              </button>
          ))}
          <button className={styles.zeroNumber} onClick={() => onClickNumber(0)}>
            0
          </button>
          <button onClick={() => onClick('.', 0)}>.</button>
        </div>
        <div className={styles.opKeyContainer}>
          <button onClick={() => onClick('+', 0)}>+</button>
          <button onClick={() => onClick('-', 0)}>-</button>
          <button onClick={() => onClick('x', 0)}>x</button>
          <button onClick={() => onClick('/', 0)}>/</button>
          <button onClick={() => onClick('√', 0)}>√</button>
          <button onClick={() => onClick('Σ', 0)}>Σ</button>
          <button onClick={() => onClick('Undo', 0)}>Undo</button>
          <button onClick={() => onClick('Intro', 0)}>Intro</button>
      </div>
      <div className={styles.stack}>{stack.map(renderStackItem)}</div>
    </div>
  );
};
