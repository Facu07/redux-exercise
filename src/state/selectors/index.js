export const selectCurrentNumber = (state) => {
  return state.operatorNumber;
};

export const selectCurrentStack = (state) => {
  return state.stack;
};

//><