import {add, sustract, multiply, divide, sqrt, sum} from 'state/math';

export const operations = (action, number) => ({
  type: action,
  number: number,
});

export const OPERATION_ADD = (state) => {
  return {
    operatorNumber: 0,
    stack: [(add(state.stack.shift(), state.operatorNumber)), ...state.stack],
    lastState: [...state.lastState, state]
  };
};

export const OPERATION_SUBTRACT = (state) => {
  return {
    operatorNumber: 0,
    stack: [(sustract(state.stack.shift(), state.operatorNumber)), ...state.stack],
    lastState: [...state.lastState, state]
  };
};

export const OPERATION_MULTIPLY = (state) => {
  return {
    operatorNumber: 0,
    stack: [(multiply(state.stack.shift(), state.operatorNumber)), ...state.stack],
    lastState: [...state.lastState, state]
  };
};


export const OPERATION_DIVISION = (state) => {
  if(parseFloat(state.operatorNumber) !== 0.0) {
    return {
      operatorNumber: 0,
      stack: [(divide(state.stack.shift(), state.operatorNumber)), ...state.stack],
      lastState: [...state.lastState, state]
    };
  }
  return state;
};

export const OPERATION_SQUARE_ROOT = (state) => {
  if(state.operatorNumber > 0) {
    return {
      operatorNumber: 0,
      stack: [(sqrt(state.operatorNumber)), ...state.stack],
      lastState: [...state.lastState, state]
    };
  }else{
      return {
        operatorNumber: 0,
        stack: [(sqrt(state.stack.shift())), ...state.stack],
        lastState: [...state.lastState, state]
      };
  }
};

export const OPERATION_SUM = (state) => {
  return {
    operatorNumber: 0,
    stack: [Math.round((parseFloat(state.operatorNumber) + (sum(state.stack)))*100)/100],
    lastState: [...state.lastState, state]
  };
};

export const OPERATION_UNDO = (state) => {
  if (state.lastState !== undefined){
    return {
      ...state.lastState.pop()
    };
  }
  return {
    operatorNumber: state.operatorNumber,
    stack: [0],
    lastState: [{
      operatorNumber: 0,
      stack: [0],
      }],
  };
};

export const OPERATION_INTRO = (state) => {
  if(state.operatorNumber !== 0) {
    return {
      operatorNumber: 0,
      stack: [Math.round((parseFloat(state.operatorNumber))*100)/100, ...state.stack],
      lastState: [...state.lastState, state]
    };
  }
  return state;
};

export const DOT_ADDING = (state) => {
  if((state.operatorNumber.toString()).indexOf('.') === -1) {
    return {
      operatorNumber: state.operatorNumber.toString() + '.',
      stack: state.stack,
      lastState: [...state.lastState, state]
    };
  }
  return state;
};

export const ADD_NUMBER = (state, action) => {
	if(state.operatorNumber === 0){
    return {
      operatorNumber: action.number,
      stack: state.stack,
      lastState: [...state.lastState, state]
    };
  }else {
    return {
      operatorNumber: state.operatorNumber.toString() + action.number.toString(),
      stack: state.stack,
      lastState: [...state.lastState, state]
    };
  }
};