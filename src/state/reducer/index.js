import {OPERATION_ADD, OPERATION_SUBTRACT, OPERATION_MULTIPLY, OPERATION_DIVISION, OPERATION_SQUARE_ROOT, OPERATION_SUM, OPERATION_UNDO, OPERATION_INTRO, DOT_ADDING, ADD_NUMBER} from 'state/actions';

export const rootReducer = (state, action) => {
  if (state === undefined) {
    return {
      operatorNumber: 0,
      stack: [0],
      lastState: [{
        operatorNumber: 0,
        stack: [0],
      }],
    };
  }

  switch (action.type) {
    case '+':
      return OPERATION_ADD(state);
    case '-':
      return OPERATION_SUBTRACT(state);
    case 'x':
      return OPERATION_MULTIPLY(state);
    case '/':
      return OPERATION_DIVISION(state);
    case '√':
      return OPERATION_SQUARE_ROOT(state);
    case 'Σ':
      return OPERATION_SUM(state);
    case 'Undo':
      return OPERATION_UNDO(state);
    case 'Intro':
      return OPERATION_INTRO(state);
    case '.':
      return DOT_ADDING(state);
    case '':
      return ADD_NUMBER(state, action);
    default:
      return state;
  }

};