export const add = (first, second) => Math.round((parseFloat(first) + parseFloat(second))*100) / 100;

export const sustract = (first, second) =>  Math.round((parseFloat(first) - parseFloat(second))*100) / 100;

export const multiply = (first, second) =>  Math.round((parseFloat(first) * parseFloat(second)*100) / 100);

export const divide = (first, second) =>  Math.round((parseFloat(first) / parseFloat(second))*100) / 100;

export const sqrt = (first) => {
	if (parseFloat(first) > 0) {
		return (Math.round((Math.sqrt(parseFloat(first)))*100) / 100);
	}
	return (Math.round((parseFloat(first))*100) / 100);
};

export const sum = (numeros) => numeros.reduce((a, b) => a += b);

export const power = (first) => Math.round(Math.pow(parseFloat(first), 2.0));
